const d3 = require('d3');
const jsdom = require('jsdom');

const args = require('minimist')(process.argv.slice(2));

const { document } = (new jsdom.JSDOM()).window,
	svg = d3.select(document.body)
		.append('svg')
			// .attr('width', args.w)
			// .attr('height', args.h);

const locale = d3.formatLocale({
	decimal: ",",
	thousands: " ",
	grouping: [3],
	currency: ["", " Kč"],
	minus: "\u2212",
	percent: "\u202f%"
});
const localeFormat = locale.format(',.0f');

const chunks = [];

process.stdin.on('readable', () => {
	let chunk;
	while (null !== (chunk = process.stdin.read())) {
		chunks.push(chunk);
	}
});
	
process.stdin.on('end', () => {
	const input = chunks.join('');
	const data = JSON.parse(input);

	const projection = d3.geoIdentity()
		.fitSize([args.w, args.h], data);

	const scale_length = args.l * 1,
		scale_label = `${scale_length / args.d} ${args.unit}`,
		scale_width = scale_length * projection.scale();

	const g = svg.append('g')
		.attr('transform', `translate(${args.x * 1},${args.y * 1 + 0.5})`);
		// .attr('transform', `translate(${args.x * 1 - scale_width / 2},${args.y * 1 + 0.5})`);

	g.append('line')
		.attr('x1', -scale_width/2)
		.attr('x2', scale_width/2)
		.style('stroke-width', 1.5)
		.style('stroke', '#666')
		// .attr('shape-rendering', 'crispEdges')

	g.append('text')
		.attr('y', -6)
		.attr('text-anchor', 'middle')
		.attr('font-family', 'Source Sans Pro')
		.attr('font-size', 14)
		.style('fill', 'black')
		.text(scale_label);

	process.stdout.write(svg.html());
});
