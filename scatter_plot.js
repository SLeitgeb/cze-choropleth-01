const d3 = require('d3');
const jsdom = require('jsdom');

const margin = {top: 10, right: 30, bottom: 30, left: 60},
	width = 460 - margin.left - margin.right,
	height = 400 - margin.top - margin.bottom;

const { document } = (new jsdom.JSDOM()).window
	svg = d3.select(document.body)
		.append('svg')
			.attr('width', width + margin.left + margin.right)
			.attr('height', height + margin.top + margin.bottom)
		.append('g')
			.attr('transform',
				`translate(${margin.left}, ${margin.top})`);

const chunks = [];

process.stdin.on('readable', () => {
	let chunk;
	while (null !== (chunk = process.stdin.read())) {
		chunks.push(chunk);
	}
});
	
process.stdin.on('end', () => {
	const input = chunks.join('');
	const data = d3.csvParseRows(input);

	const x = d3.scaleLog()
		.domain([1,500])
		.range([ 0, width ]);
	svg.append('g')
		.attr('transform', `translate(0, ${height})`)
		.call(d3.axisBottom(x));

	const y = d3.scaleLog()
		.domain([1,3000])
		.range([ height, 0 ]);
	svg.append('g')
		.call(d3.axisLeft(y));

	svg.append('g')
		.selectAll('dot')
		.data(data)
		.enter()
		.append('circle')
			.attr('cx', d => x(d[0]))
			.attr('cy', d => y(d[1]))
			.attr('r', 2)
			.style('fill', 'none')
			.style('stroke', 'steelblue')
			.style('stroke-width', 1);

	process.stdout.write(document.body.innerHTML);
});
