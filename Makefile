SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

ndjson-cat-xl = node --max-old-space-size=10000 `which ndjson-cat`
ndjson-split-xl = node --max-old-space-size=10000 `which ndjson-split`

WIDTH = 960
HEIGHT = 550
YEAR = 2019
# PLOT_RANGE = [1, 10, 50, 200, 500, 1000, 2000]
PLOT_RANGE = [1, 10, 40, 100, 250, 500, 1000]

all:

data:
	mkdir -p $@

data/admin.zip: | data
	wget -O $@ http://services.cuzk.cz/gml/inspire/au/epsg-5514/1.zip

data/stat.zip: | data
	wget -O $@ https://www.czso.cz/documents/11300/0/multi%C4%8D%C3%ADseln%C3%ADky+UTJ_ZSJD_20190101.zip/a54a94e5-b5a2-47e6-818d-215bf870acea?version=1.0

data/pop_LAU.zip: | data
	wget -O $@ https://www.czso.cz/documents/62353418/123503123/340129-20data063020.zip

data/pop_NUTS.csv: | data
	wget -O $@ https://www.czso.cz/documents/62353418/123502807/130142-20data051520.csv

data/pop_LAU.ndjson: data/pop_LAU.zip
	unzip $<
	# DEM0026 - počet obyvatel, viz https://www.czso.cz/documents/62353418/114658260/340129-19dds.htm
	csvgrep -c vuk_id -m DEM0026 OD_UAP01_2020063018272760.CSV \
	| csvcut -c uzemi_kod,hodnota,prislorp_kod \
	| csvjson \
	| ndjson-split \
	| ndjson-map '{id: d.uzemi_kod, POPULATION: d.hodnota, COUNTY: d.prislorp_kod}' \
	> $@
	rm OD_UAP01_2020063018272760.CSV

data/pop_NUTS3.csv: data/pop_NUTS.csv
	csvgrep -c vuzemi_cis -m 100 < $< \
	| csvgrep -c casref_do -m $(YEAR)-12-31 \
	| csvgrep -c pohlavi_kod,vek_kod -r '^$$' \
	> $@

data/admin.xml: data/admin.zip
	unzip $<
	mv 1.xml $@
	touch $@

data/NUTS3.geojson: data/admin.xml
	ogr2ogr \
		-where "LocalisedCharacterString = 'Kraj'" \
		-s_srs EPSG:5514 \
		-t_srs EPSG:32633 \
		-f GeoJSON $@ \
		$< AdministrativeUnit

data/LAU.geojson: data/admin.xml
	ogr2ogr \
		-where "LocalisedCharacterString = 'Obec'" \
		-s_srs EPSG:5514 \
		-t_srs EPSG:32633 \
		-f GeoJSON $@ \
		$< AdministrativeUnit

data/admin.geojson: data/admin.xml
	ogr2ogr -s_srs EPSG:5514 -t_srs EPSG:32633 -f GeoJSON $@ $< AdministrativeUnit

data/%_export.geojson: data/%.ndjson
	ndjson-reduce \
	< $< \
	| ndjson-map '{type: "FeatureCollection", features: d}' \
	> $@

data/%.ndjson: data/%.geojson
	mapshaper -i $< \
		-each 'AREA = this.originalArea' \
		-o - \
	| $(ndjson-cat-xl) \
	| $(ndjson-split-xl) 'd.features' \
	| ndjson-map 'd.id = d.properties.nationalCode, d' \
	> $@

data/%_density.ndjson: data/%.ndjson data/pop_%.ndjson
	ndjson-join 'd.id' \
		$^ \
	| ndjson-map 'd[0].properties = {DENSITY: Math.floor(d[1].POPULATION / d[0].properties.AREA * 1000000), COUNTY: d[1].COUNTY, AREA: d[0].properties.AREA}, d[0]' \
	> $@

data/%_topo.json: data/%_density.ndjson
	geo2topo \
		tracts=<(ndjson-reduce \
			< $< \
			| ndjson-map '{type: "FeatureCollection", features: d}' \
			| geoproject \
			'd3.geoIdentity().reflectY(true).fitSize([$(WIDTH), $(HEIGHT)], d)') \
	| toposimplify -p 1 -f \
	| topoquantize 1e5 \
	> $@

data/%_mesh_topo.json: data/%_topo.json
	topomerge -k 'd.properties.COUNTY' counties=tracts \
	< $< \
	| topomerge --mesh -f 'a !== b' counties=counties \
	> $@

%_legend.svg:
	node legend.js \
	--range='$(PLOT_RANGE)' \
	-w $(WIDTH) \
	-h $(HEIGHT) \
	> $@

data/%_bbox.geojson: data/%.geojson
	mapshaper \
		-i $< \
		-rectangle source=* \
		-o format=geojson - \
	> $@

%_scalebar.svg: data/%_bbox.geojson
	cat $< \
	| node scalebar.js \
		-x 102 \
		-y 490 \
		-w $(WIDTH) \
		-h $(HEIGHT) \
		-l 50000 \
		-d 1000 \
		--unit km \
	> $@

%_map.svg: data/%_topo.json data/%_mesh_topo.json 
	(topo2geo tracts=- \
		< $< \
		| ndjson-map -r d3 -r d3-scale-chromatic 'z = 
		d3.scaleThreshold().domain($(PLOT_RANGE)).range(d3.schemeOrRd[8]),
		d.features.forEach(f => f.properties.fill =
		z(f.properties.DENSITY)), d' \
		| ndjson-split 'd.features'; \
		topo2geo counties=- \
		< data/$*_mesh_topo.json \
		| ndjson-map 'd.properties = {"stroke": "#000", "stroke-opacity": 0.3}, d')\
	| geo2svg -n --stroke none -p 1 -w $(WIDTH) -h $(HEIGHT) \
	| sed '$$d' \
	> $@

%.svg: %_map.svg %_legend.svg %_scalebar.svg
	cat $^ \
	> $@
	echo '</svg>' \
	>> $@

%.png: %.svg
	inkscape \
		-d 300 \
		-b "#fff" \
		-y 1 \
		$< -o $@ 

# GRAPH
data/%_scatter_data.csv: data/%_density.ndjson
	ndjson-map '{x: d.properties.AREA / 1000000, y: d.properties.DENSITY}' \
	< $< \
	| json2dsv -n \
	> $@

%_scatter.svg: data/%_scatter_data.csv
	cat $< \
	| node scatter_plot.js \
	> $@

clean:
	rm -rf data/admin.zip data/admin.xml data/stat.zip

.SECONDARY:
.PHONY: all clean
.DELETE_ON_ERROR:
