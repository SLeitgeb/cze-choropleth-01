const d3 = require('d3');
const jsdom = require('jsdom');

const args = require('minimist')(process.argv.slice(2));
const plot_range = JSON.parse(args.range);

const { document } = (new jsdom.JSDOM()).window,
	svg = d3.select(document.body)
		.append('svg')
			.attr('width', args.w)
			.attr('height', args.h);

// svg.append('defs')
// 	.append('style')
// 	.attr('type', 'text/css')
// 	.html(`<![CDATA[
// 	text {
// 		font-family: "Source Sans Pro";
// 	}
// 	]]>`)

const locale = d3.formatLocale({
	decimal: ",",
	thousands: " ",
	grouping: [3],
	currency: ["", " Kč"],
	minus: "\u2212",
	percent: "\u202f%"
});
const localeFormat = locale.format(',.0f');

const threshold = d3.scaleThreshold()
	.domain(plot_range)
	.range(d3.schemeOrRd[plot_range.length + 1]);

const x = d3.scaleSymlog()
	.constant(10)
	.domain([0, 2670])
	.range([0, 350]);

const xAxis = d3.axisBottom(x)
	.tickSize(13)
	.tickValues(threshold.domain())
	// .tickFormat(localeFormat);
	.tickFormat(d => d === plot_range.slice(-1)[0] ? `${localeFormat(d)} ob. / km²` : localeFormat(d));

const g = svg.append('g')
	.attr('transform', 'translate(580,30)')
	.call(xAxis);

g.select('.domain')
	.remove();

g.selectAll('rect')
	.data(threshold.range().map(color => {
		const d = threshold.invertExtent(color);
		if (d[0] === undefined) d[0] = x.domain()[0];
		if (d[1] === undefined) d[1] = x.domain()[1];
		return d;
	}))
	.enter().insert('rect', '.tick')
		.attr('height', 8)
		.attr('x', d => x(d[0]))
		.attr('width', d => x(d[1]) - x(d[0]))
		.attr('fill', d => threshold(d[0]));

g.append('text')
	.attr('fill', '#000')
	.attr('font-weight', 'bold')
	.attr('text-anchor', 'start')
	.attr('y', -6)
	.text('Hustota obyvatelstva')
  
d3.select(g.selectAll('text')
	.nodes()
	.filter(node => node.__data__ === plot_range.slice(-1)[0])[0])
	.style('text-anchor', 'start')
	.attr('transform', 'translate(-17,0)');

process.stdout.write(`<defs><style type="text/css">
	text {
		font-family: "Source Sans Pro";
	}
</style></defs>
${svg.html()}`);
